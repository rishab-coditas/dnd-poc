import { IAction, IThunkAction } from "interfaces";
import { store } from "@store/index";

export function dispatch<T extends IAction>(action: T | IThunkAction) {
  if ((action as IAction).type) {
    return store.dispatch(action as IAction);
  }
  return store.dispatch<{ type: string }>(action as IThunkAction);
}
export function isEmpty(obj: Object) {
  if (Object.keys(obj).length <= 0 || !obj) {
    return true;
  }
  return false;
}
export const errorMessages = (key) => {
  switch (key) {
    case "uname":
      return "Please enter your full name.";
    case "email":
      return "Please enter your email address.";
    case "validEmail":
      return "Please enter a valid email address.";
    case "password":
      return "Please enter a password";
    case "confirmPassword":
      return "Please enter a confirm password";
    case "emptyconfirmPassword":
      return "Please enter a confirm password";
    case "nonMatchingPasswords":
      return "Passwords don't match";
    default:
      return;
  }
};
export const validNumberInput = (data) => {
  const text = data;
  const regex = /^\d{0,}(\.\d{0,6})?$/;
  var test = regex.test(text);
  return test;
};
export const validYear = (data) => {
  const text = data;
  const regex = /^\d{0,}(\.\d{0,4})?$/;
  var test = regex.test(text);
  return test;
};
export const validateEmail = (email) => {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
};
export const passwordTitle = `
• Must be atleast 6 characters long
• Can have atleast one or more :
    • one lowercase character
    • one uppercase character
    • one numeric character
    • one special character
`;

export const strongRegex = new RegExp(
  "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{8,})"
);
export const mediumRegex = new RegExp(
  "^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{6,})"
);