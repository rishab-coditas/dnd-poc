import React, { PureComponent } from "react";
// import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import { NavBar } from "@components/NavBar/Navbar";
import { Icon } from "@material-ui/core";
import "./styles/Documentation.style.scss";
import JsonEditor from "../../components/Editor/JsonEditor";
import CKEditor from '@ckeditor/ckeditor5-react';
import Editor from '../../components/Editor/ckeditor5/build/ckeditor';

const getItems = () => [
	{
		id: "header",
		content: "Header",
		icon: "note_add"
	},
	{
		id: "code",
		content: "Code Editor",
		icon: "code"
	},
	{
		id: "url",
		content: "Embeded",
		icon: "http"
	},
	{
		id: "img",
		content: "Image",
		icon: "image"
	},
	{
		id: "label",
		content: "Callout",
		icon: "error_outline"
	}
];

// a little function to help us with reordering the result
const reorder = (list: Iterable<unknown> | ArrayLike<unknown>, startIndex: number, endIndex: number) => {
	const result = Array.from(list);
	const [removed] = result.splice(startIndex, 1);
	result.splice(endIndex, 0, removed);

	return result;
};

export interface IDocInterface {
	onChange?: any;
	items?: any;
	box?: any;
	value?: any;
	deleteAll?: any;
	insertLabel?: any;
}

// const Size = Quill.import("formats/size");
// Size.whitelist = ["extra-small", "small", "medium", "large"];
// Quill.register(Size, true);

// // Add fonts to whitelist and register them
// const Font = Quill.import("formats/font");
// Font.whitelist = [
// 	"arial",
// 	"comic-sans",
// 	"courier-new",
// 	"georgia",
// 	"helvetica",
// 	"lucida"
// ];
// Quill.register(Font, true);

// const CustomToolbar = () => (
// 	<div id="toolbar">
// 		<select className="ql-header" defaultValue={""} onChange={e => e.persist()}>
// 			<option value="1" />
// 			<option value="2" />
// 			<option value="" />
// 		</select>
// 		<select className="ql-font">
// 			<option value="arial" selected>
// 				Arial
// 		</option>
// 			<option value="comic-sans">Comic Sans</option>
// 			<option value="courier-new">Courier New</option>
// 			<option value="georgia">Georgia</option>
// 			<option value="helvetica">Helvetica</option>
// 			<option value="lucida">Lucida</option>
// 		</select>
// 		<button className="ql-bold" />
// 		<button className="ql-italic" />
// 		<button className="ql-underline" />
// 		<button className="ql-strike" />
// 		<button className="ql-image" />
// 		<button className="ql-video" />
// 		<button className="ql-link" />
// 		<select className="ql-align" />
// 		<select className="ql-color" />
// 		<select className="ql-background" />
// 		<button className="ql-clean" />
// 		<button className="ql-jsonEditor">
// 			<Icon>code</Icon>
// 			{/* <JsonEditor /> */}
// 		</button>
// 	</div>
// );

// const styleMap = {
//     'STRIKETHROUGH': {
//         textDecoration: 'line-through',
//     },
// };

export class Documentation extends PureComponent<any, IDocInterface> {
	constructor(props: IDocInterface) {
		super(props);
		this.state = {
			items: getItems(),
			box: [],
			value: '<h1>Start writing content here... </h1>'
		};
	}

	onDragEnd(result: { destination: { index: any; }; source: { index: any; }; }) {
		const { items }: any = this.state;
		if (!result.destination) {
			return;
		}
		const item = reorder(items, result.source.index, result.destination.index);
		// const newItems = items.filter((item) => item.id !== result.draggableId);
		this.setState({
			items: item
		});
	}

	dragStart = (e: { draggableId: any; }) => {
		console.log(">> e", e);
		const { box, items }: any = this.state;
		const newItems = items.filter((item: { id: any; }) => item.id !== e.draggableId);
		box.push(e);
		this.setState({
			box,
			items: newItems
		});
	};

	insertLabel = (data: any) => {
		console.log('>> data', data);
	}

	removeWidget = (item: string) => {
		alert("Are you sure you want to delete? All content will be erased");
		console.log(">> item", item);
		const { items, box }: any = this.state;
		const newItem = getItems().find((i) => i.id === item);
		items.push(newItem);
		const newBoxCont = box.filter((i: { draggableId: any; }) => i.draggableId !== item);
		box.push(newBoxCont);
		console.log(">> newItem", newItem);
		this.setState({
			items,
			box
		});
	};

	onChange = (evt: any) => {
		this.setState({
			value: evt.editor.getData()
		});
	};

	render() {
		const { items, value, box }: any = this.state;
		console.log(">> box", items, value);
		// const modules = {
		// 	ImageResize: {
		// 		modules: ['Resize', 'DisplaySize', 'Toolbar']
		// 	},
		// 	// keyboard: {
		// 	// 	bindings: QuillBetterTable.keyboardBindings
		// 	// },
		// 	// toolbar: [
		// 	// 	[{ 'header': '1' }, { 'header': '2' }, { 'font': [] }],
		// 	// 	[{ size: [] }],
		// 	// 	['bold', 'italic', 'underline', 'strike', 'blockquote'],
		// 	// 	[{ 'list': 'ordered' }, { 'list': 'bullet' },
		// 	// 	{ 'indent': '-1' }, { 'indent': '+1' }],
		// 	// 	['link', 'image', { 'color': [] }, { 'background': [] }, 'code-block'],
		// 	// 	['clean']
		// 	// ],
		// 	toolbar: {
		// 		container: "#toolbar",
		// 		handlers: {
		// 			jsonEditor: insertHeart
		// 		}
		// 	},
		// 	clipboard: {
		// 		// toggle to add extra line breaks when pasting HTML:
		// 		matchVisual: false,
		// 	}
		// }
		// const formats =
		// 	[
		// 		'header', 'font', 'size',
		// 		'bold', 'italic', 'underline', 'mark', 'strike', 'blockquote', 'list', 'bullet', 'indent',
		// 		'link', 'image', 'color', 'background', 'code-block'
		// 	]

		// const config = [
		// 	{ name: 'document', items: ['Source', '-', 'Save', 'NewPage', 'Preview', 'Print', '-', 'Templates'] },
		// 	{ name: 'clipboard', items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'] },
		// 	{ name: 'editing', items: ['Find', 'Replace', '-', 'SelectAll', '-', 'Scayt'] },
		// 	{ name: 'forms', items: ['Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField'] },
		// 	{ name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'CopyFormatting', 'RemoveFormat'] },
		// 	{ name: 'paragraph', items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language'] },
		// 	{ name: 'links', items: ['Link', 'Unlink', 'Anchor'] },
		// 	{ name: 'insert', items: ['Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe'] },
		// 	{ name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize'] },
		// 	{ name: 'colors', items: ['TextColor', 'BGColor'] },
		// 	{ name: 'tools', items: ['Maximize', 'ShowBlocks'] },
		// 	{ name: 'about', items: ['About'] }
		// ];


		return (
			<div className="main-div">
				<NavBar />
				<div className="main-section">
					<section className="sidebar">
						<p>Side bar</p>
					</section>
					<div className="droppables">
						{/* <Editor  editorState={value} onChange={this.onChange} customStyleMap={styleMap} /> */}
						<div className="rte">
							{/* <CustomToolbar />
							<ReactQuill
								theme="snow"
								onChange={e => this.setState({ value: e })}
								value={value}
								formats={formats}
								modules={modules}
								placeholder='Start documenting here...'
							// readOnly={this.props.readOnly}
							// placeholder="Research..." 
							/> */}
							<CKEditor
								onInit={editor => console.log('Editor is ready to use!', editor)}
								onChange={(event, editor) => this.setState({ value: editor.getData() })}
								config={{
									plugins: Editor.builtinPlugins,
									// toolbar: ClassicEditor.defaultConfig.toolbar
									toolbar: {
										items: [
											'heading',
											'|',
											'bold',
											'italic',
											'link',
											'bulletedList',
											'numberedList',
											'|',
											'indent',
											'outdent',
											'|',
											'imageUpload',
											'blockQuote',
											'insertTable',
											'mediaEmbed',
											'undo',
											'redo',
											'alignment',
											'code',
											'codeBlock',
											'fontBackgroundColor',
											'fontColor',
											'fontSize',
											'highlight',
											'fontFamily',
											'pageBreak',
											'horizontalLine',
											'removeFormat',
											'restrictedEditingException',
											'specialCharacters',
											'strikethrough',
											'underline'
										]
									},
									language: 'en',
									image: {
										toolbar: [
											'imageTextAlternative',
											'imageStyle:full',
											'imageStyle:side'
										]
									},
									table: {
										contentToolbar: [
											'tableColumn',
											'tableRow',
											'mergeTableCells',
											'tableCellProperties',
											'tableProperties'
										]
									},
									licenseKey: '',
								}}
								editor={Editor}
								data={value}
							/>
							<JsonEditor />
						</div>
						{box?.map((item: { draggableId: string; }, i: string | number | undefined) => {
							return (
								<div className="widget" key={i}>
									<div className="widget-items">
										{item.draggableId === "img" && (
											<div className="form-group">
												<label htmlFor="example1">Upload Image</label>
												<input type="file" id="example1" className="form-control form-control-lg" />
											</div>
										)}
										{item.draggableId === "code" && (
											// <JsonEditor />
											<div className="form-group">
												<label htmlFor="code">Code-Editor</label>
												<JsonEditor />
											</div>
										)}
										{item.draggableId === "url" && (
											<div className="form-group">
												<label htmlFor="url">API-Endpoint</label>
												<input type="text" id="url" className="form-control form-control-md" />
											</div>
										)}
										{item.draggableId === "label" && (
											<div className="form-group">
												<label htmlFor="lebel">Callout</label>
												<textarea className="form-control" id="lebel" />
											</div>
										)}
										{item.draggableId === "header" && (
											<div className="header-inputs">
												<div className="form-group input-div">
													<input type="text" id="h1" className="form-control form-control-sm" />
												</div>
												<div className="form-group input-div">
													<input type="text" id="h2" className="form-control form-control-sm" />
												</div>
												<div className="form-group input-div">
													<input type="text" id="h3" className="form-control form-control-sm" />
												</div>
												<div className="form-group input-div">
													<input type="text" id="h4" className="form-control form-control-sm" />
												</div>
												<div className="form-group input-div">
													<input type="text" id="h5" className="form-control form-control-sm" />
												</div>
												<div className="form-group input-div">
													<input type="text" id="h6" className="form-control form-control-sm" />
												</div>
											</div>
										)}
									</div>
									<div className="icon-cont">
										<button
											className="btn"
											title="Clear"
											onClick={() => console.log(item.draggableId)}
										>
											<Icon className="clear">clear</Icon>
										</button>
										<button
											className="btn"
											title="Delete"
											onClick={() => this.removeWidget(item.draggableId)}
										>
											<Icon className="delete">delete_forever</Icon>
										</button>
									</div>
								</div>
							);
						})}
						{box.length > 5 && (
							<div className="btn">
								<button type="button" className="btn btn-outline-primary">
									Save
                </button>
							</div>
						)}
					</div>
					{/* <section className="draggable">
						<DragDropContext
							// onDragStart={e => this.dragStart(e)}
							onDragEnd={this.dragStart}
						>
							<Droppable droppableId="droppable">
								{(provided, snapshot) => (
									<div
										{...provided.droppableProps}
										ref={provided.innerRef}
										style={{ display: "flex", flexWrap: "wrap", width: "14em" }}
									>
										{items.map((item, index) => (
											<Draggable key={item.id} draggableId={item.id} index={index}>
												{(provided, snapshot) => (
													<div
														ref={provided.innerRef}
														{...provided.draggableProps}
														{...provided.dragHandleProps}
														style={getItemStyle(snapshot.isDragging, provided.draggableProps.style)}
													>
														<div
															className="card"
															style={{
																display: "flex",
																flexDirection: "column",
																justifyContent: "center",
																alignItems: "center",
																border: "1px dotted grey",
																width: "4em",
																height: "5em"
															}}
														>
															<Icon style={{ fontSize: "20px", textAlign: "center" }}>
																{item.icon}
															</Icon>
															<p style={{ fontSize: "12px", textAlign: "center", margin: "0" }}>
																{item.content}
															</p>
														</div>
													</div>
												)}
											</Draggable>
										))}
										{provided.placeholder}
									</div>
								)}
							</Droppable>
						</DragDropContext>
					</section> */}
				</div>
			</div>
		);
	}
}
