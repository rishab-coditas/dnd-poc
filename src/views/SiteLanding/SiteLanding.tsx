import React, { PureComponent } from 'react';
import { NavBar } from '@components/NavBar/Navbar';
import './styles/Sitelanding.style.scss';
import Icon from '@material-ui/core/Icon';
import { Link } from 'react-router-dom';

export class Sitelanding extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            param: props?.match?.url === '/cars' ? 'Cars' : 'Hotels',
        }
    }

    render() {
        console.log('>> props', this.props);
        const { param }: any = this.state;
        return (
            <div className='head-div'>
                <NavBar />
                <section className='sitename-section'>
                    <p>{param}</p>
                </section>
                <section className='content-section'>
                    <div className='card'>
                        <Icon className="icon">filter_none</Icon>
                        <h2>Create API Reference</h2>
                        <p>Comprehensive documentation of our API endpoints with an interactive Try It Now feature.</p>
                        <Link to="/api-reference">
                            <button>Create API Reference</button>
                        </Link>
                    </div>
                </section>
                <section>
                    <p>Footer</p>
                </section>
            </div>
        )
    }
}