import React, { PureComponent } from 'react';
import { NavBar } from '@components/NavBar/Navbar';
import './styles/SitesScreen.style.scss';
import { Link } from 'react-router-dom';

export interface ISitesState { }

export class SitesScreen extends PureComponent<any, ISitesState> {
    constructor(props: ISitesState) {
        super(props);
    }

    render() {
        return (
            <div className="head-section">
                <div className="nav-container">
                    <NavBar />
                </div>
                <div className="img-container">
                    <img src="https://www.klifftechnologies.in/images/svg/banner-cms.png" />
                </div>
                <div className='img-section'>
                    <Link className="title-img" to="/cars">
                        <img
                            src="https://wallpapercave.com/wp/dTDNPcf.jpg"
                        />
                        <p>Cars</p>
                    </Link>
                    <Link className="title-img" to="/hotels">
                        <img
                            src="https://pix10.agoda.net/hotelImages/124/1246280/1246280_16061017110043391702.jpg?s=1024x768"
                        />
                        <p>Hotels</p>
                    </Link>
                </div>
                <div className="desc-section">
                    <h3>Description</h3>
                    <p>Some Description.</p>
                </div>
                <div className='footer-section'>
                    <h4>
                        Footer
                    </h4>
                </div>
            </div>
        )
    }
}