import axios from "axios";
import { showLoader } from "@actions/miscellaniousActions";
import { PROD_BASE_URL, PROD_URL, DEV_URL } from "@constants/urls";

export const WEBSOCKET_URL = process.env.NODE_ENV === "production" ? PROD_URL : DEV_URL;

export const LOCALSTORAGE_AUTH_KEY = "authorization";
export const HEADER_AUTH_KEY = "authorization";

const axiosInstance = axios.create({
  baseURL: (process.env.NODE_ENV === "production" && PROD_URL) || DEV_URL,
  headers: {
    "Content-Type": "application/json",
    "Access-Control-Allow-Origin": PROD_BASE_URL || DEV_URL,
    'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, PATCH, OPTIONS',
    [HEADER_AUTH_KEY]: localStorage.getItem(LOCALSTORAGE_AUTH_KEY)
  }
});

export class HTTPService {
  static get(url, params = null): Promise<any> {
    return new Promise((resolve, reject) => {
      axiosInstance.interceptors.request.use((config) => {
        config.headers[HEADER_AUTH_KEY] = localStorage.getItem(LOCALSTORAGE_AUTH_KEY) || "";
        return config;
      });
      axiosInstance
        .get(url, { params: params })
        .then((response) => {
          resolve(response.data);
        })
        .catch((error) => reject(error));
    });
  }

  static put(url, body): any {
    return new Promise((resolve, reject) => {
      showLoader(true);
      axiosInstance.interceptors.request.use((config) => {
        config.headers[HEADER_AUTH_KEY] = localStorage.getItem(LOCALSTORAGE_AUTH_KEY);
        return config;
      });
      axiosInstance
        .put(url, body)
        .then((response) => {
          resolve(response.data);
        })
        .catch((error) => {
          if (error.response) {
            reject(error.response.data);
            return;
          }
          reject(error);
        });
    });
  }

  static post(url, body, headers = {}): any {
    return new Promise((resolve, reject) => {
      // showLoader(true);
      axiosInstance.interceptors.request.use((config) => {
        config.headers[HEADER_AUTH_KEY] = localStorage.getItem(LOCALSTORAGE_AUTH_KEY);
        return { ...config, headers: { ...config.headers, ...headers } };
      });
      axiosInstance
        .post(url, body)
        .then((response) => {
          if (response.data.statusCode !== (200 || 201)) {
            // showLoader(false);
            console.log(response.data, "response")

            // reject(response.data);
            throw response.data;
          }
          // showLoader(false);
          resolve(response.data);
        })
        .catch((error) => {
          // showLoader(false);
          if (error.response.data) {
            reject(error.response.data);
          }
        });
    }).catch((error) => {
      return error
    });
  }

  static delete(url) {
    return new Promise((resolve, reject) => {
      showLoader(true);
      axiosInstance.interceptors.request.use((config) => {
        config.headers[HEADER_AUTH_KEY] = localStorage.getItem(LOCALSTORAGE_AUTH_KEY);
        return config;
      });
      axiosInstance
        .delete(url)
        .then((response) => {
          if (response.status === 200) {
            showLoader(false);
            resolve(response.data);
          }
        })
        .catch((error) => reject(error));
    });
  }
}
export function saveToken(response) {
  if (response && response[HEADER_AUTH_KEY]) {
    localStorage.setItem(LOCALSTORAGE_AUTH_KEY, response[HEADER_AUTH_KEY]);
  }
}
