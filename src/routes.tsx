import React, { PureComponent } from "react";
import { connect } from "react-redux";
import { Route, Switch } from "react-router";
import { BrowserRouter as Router } from "react-router-dom";

import { SitesScreen } from "@views/Sites/SitesScreen";
import { Sitelanding } from "@views/SiteLanding/SiteLanding";
import { Documentation } from "@views/Documentation/Documentation";

export class RoutesComponent extends PureComponent {
  state = { isLoading: false };

  render() {
    const history = require('history').createBrowserHistory;
    if (this.state.isLoading) {
      return <div>Loading...</div>;
    }
    return (
      <Router history={history}>
        <Switch>
          <Route exact path="/" component={SitesScreen} />
          <Route exact path="/cars" component={Sitelanding} />
          <Route exact path="/hotels" component={Sitelanding} />
          <Route path="/api-reference" component={Documentation} />
        </Switch>
      </Router>
    );
  }
}
const mapStateToProps = (state) => {
  return {};
};

// tslint:disable-next-line:variable-name
const Routes = connect(mapStateToProps)(RoutesComponent);

export default Routes;
