import { dispatch } from "@utils/index";
import { SHOW_LOADER } from "@constants/actions";

export function showLoader(isLoading) {
  return dispatch({
    type: SHOW_LOADER,
    isLoading
  });
}
