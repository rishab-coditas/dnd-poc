import React, { PureComponent } from 'react';
import './styles/NavBar.style.scss';
import { Link } from 'react-router-dom';

export interface INavBarState {
    title: any;
}

export class NavBar extends PureComponent<any, INavBarState> {
    constructor(props: INavBarState) {
        super(props);
        console.log('>> props', props);
    }

    render() {
        return (
            <nav className="nav-container">
                <div className="nav-section">
                    <div className='nav-img'>
                        <img
                            src="https://i2.wp.com/www.lazyfreshers.com/wp-content/uploads/2019/05/Tavisca-off-campus-drive.png?fit=200%2C200&ssl=1"
                        />
                    </div>
                    <div className='nav-content'>
                        <ul className="nav-ul">
                            <Link to="/">
                                <li>Home</li>
                            </Link>
                            <Link to="/hotels">
                                <li>Hotel</li>
                            </Link>
                            <Link to="/cars">
                                <li>Car</li>
                            </Link>
                            <li>Flight</li>
                        </ul>
                    </div>
                </div>
            </nav>
        )
    }
}