// import JsonEditor from '../../../JsonEditor';
CKEDITOR.plugins.add( 'jsonEditor', {
    icons: 'code',
    init: function( editor ) {
        editor.addCommand( 'insertJsonEditor', {
            exec: function( editor ) {
                editor.insertHtml(`<div>adding comp</div>`);
            }
        });
        editor.ui.addButton( 'Code', {
            label: 'Insert Json Editor',
            command: 'insertJsonEditor',
            toolbar: 'insert'
        });
    }
});