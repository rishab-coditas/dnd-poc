import React from 'react';
import MonacoEditor from "react-monaco-editor";
import './styles/editor.style.scss';
class JsonEditor extends React.Component<any, any> {
  editor: any;
  constructor(props) {
    super(props);
    this.state = {
      code: ["{", ' "currency": "USD",\n "programId": "docs",\n "searchQuery": { \n    "roomOccupancy": {\n        "occupants": [\n            {\n                "type": "Adult",\n                "age": 25\n            }\n        ]\n    }\n } ', "}"].join(
        "\n"
      ),
      language: "json",
      theme: "vs-dark",
      editorRef: true,
      selectedValue: ""
    };
  }
  editorWillMount = monaco => {
    console.log(monaco)
    console.log(monaco.languages.json)
    monaco.languages.json && monaco.languages.json.jsonDefaults.setDiagnosticsOptions({
      validate: true,
      schemas: [
        {
          uri: "http://myserver/foo-schema.json",
          schema: {
            type: "object",
            properties: {
              p1: {
                enum: ["v1", "v2"]
              },
              p2: {
                $ref: "http://myserver/bar-schema.json"
              }
            }
          }
        },
        {
          uri: "http://myserver/bar-schema.json",
          schema: {
            type: "object",
            properties: {
              q1: {
                enum: ["x1", "x2"]
              }
            }
          }
        }
      ]
    });
  };
  setTheme = (value) => {
    this.setState({ theme: value });
  };
  listenEditorChanges = () => {
    this.setState({ editorRef: !this.state.editorRef });
  }
  editorDidMount = editor => {
    // eslint-disable-next-line no-console
    // console.log("editorDidMount", editor, editor.getValue(), editor.getModel());
    editor.onMouseDown((e) => {
      // console.log( editor.getValue());
      this.setState({ selectedValue: e.event.target.textContent }, () => {
        // debugger;
        // console.log(e);
        console.log(e.event.target.textContent)
        console.log(e.target.element)
      });
      // console.log(e.event.target.textContent);
    });
    editor.onContextMenu(function (e) {
      console.log('contextmenu - ' + e.target.toString());
    });
    // editor.onMouseLeave(function (e) {
    //     console.log('mouseleave');
    // });
    this.editor = editor;
  };
  render() {
    const { code, language, theme, editorRef, selectedValue } = this.state;
    console.log(code)
    const options = {
      readOnly: editorRef,
      glyphMargin: true
    };
    let splitData = selectedValue.split(":");
    let value = splitData[0].replace(/[^a-zA-Z]/g, '');
    return (
      <div>
        {editorRef ?
          <button onClick={this.listenEditorChanges} >
            Try It Now
                        </button>
          :
          <button onClick={this.listenEditorChanges} >
            Switch to Sample
                        </button>
        }
        {theme === "vs-light" ?
          <button onClick={() => this.setTheme("vs-dark")} type="button">
            Set dark theme
                    </button>
          :
          <button onClick={() => this.setTheme("vs-light")} type="button">
            Set light theme
                    </button>
        }
        <hr />
        <div style={{ display: "flex" }}>
          <MonacoEditor
            width="800"
            height="300"
            language={language}
            value={code}
            editorWillMount={this.editorWillMount}
            options={options}
            theme={theme}
            editorDidMount={this.editorDidMount}
          />
          <div className="help-block">
            {value != '' ?
              <>
                <div id="isRequired" className="help-block-required">
                  <h4 className="help-block-key">{value}</h4>
                  <label className="help-block-label">required</label>
                </div>
                <i id="dataType" className="help-block-datatype">string</i>
                <div className="editor-help-text">
                  <p>Check-in date or the start date of the stay duration. The API supports same-day searches to accommodate different time zones. You can check for availability only up to 330 days in advance. Note that the availability limitation is supplier-specific. <br></br>Format: YYYY-MM-DD</p>
                  <div>
                    <div><strong>Example:</strong></div>
                    <div><label className="help-block-exaple-label">USD</label></div>
                  </div>
                </div>
              </>
              :
              <div id="noDetails" className="p10">
                <span className="help-block-header"><i>Click the JSON property to view its description.</i></span>
                <span className="help-block-body"><i>Note: If you update the sample code, right click inside the code editor and choose <strong>Format Document</strong> option to format the JSON; this will ensure that the property description appears correctly.</i></span>
              </div>
            }
          </div>
        </div>
      </div>
    );
  }
}
export default JsonEditor;